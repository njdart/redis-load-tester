package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/redis/go-redis/v9"
)

var client = redis.NewClient(&redis.Options{
	Addr:     "redis:6379",
	Password: "", // no password set
	DB:       0,  // use default DB
	MinIdleConns: 250,
})

var targetSetPerSec = 1000
var targetGetPerSec = 20000
var maxValueLength = 100
var ttl = time.Hour

var charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789-_=+[]{}<>,.?!\"£$%^&*'@#~"

var (
	buckets = []float64{5, 10, 15, 20, 25, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000}
	getDurationMSec = promauto.NewHistogram(prometheus.HistogramOpts{
		Name: "get_duration_total_msec",
		Help: "The total amount of time for redis GET operations as perceived by the load tester in milliseconds",
		Buckets: buckets,
	})
	setDurationMSec = promauto.NewHistogram(prometheus.HistogramOpts{
		Name: "set_duration_total_msec",
		Help: "The total amount of time for redis SET operations as perceived by the load tester in milliseconds",
		Buckets: buckets,
	})
)

func randomString(l int) string {
	s := ""

	for i := 0; i < l; i++ {
		s += string(charset[rand.Intn(len(charset))])
	}

	return s
}

func main() {
	wg := sync.WaitGroup{}
	for i := 0; i < targetSetPerSec; i++ {
		wg.Add(1)
		go func(i int){
			t := time.NewTicker(time.Second)
			for start := range t.C {
				key := fmt.Sprintf("worker%d:%d", i, start.Unix())
				value := randomString(rand.Intn(maxValueLength))

				ctx, cancel := context.WithTimeout(context.Background(), time.Second)
				setDurationMSec.Observe(float64(time.Since(start).Milliseconds()))
				if _, err := client.Set(ctx, key, value, ttl).Result(); err != nil {
					log.Printf("Worker %d error setting key %q to %q: %v", i, key, value, err)
				}
				cancel()
			}
		}(i)
	}

	for i := 0; i < targetGetPerSec; i++ {
		wg.Add(1)
		go func(i int){
			t := time.NewTicker(time.Second)
			for start := range t.C {
				workerId := rand.Intn(targetSetPerSec)
				randOffset := time.Duration(rand.Intn(int(ttl.Seconds()))) * time.Second
				key := fmt.Sprintf("worker%d:%d", workerId, start.Add(-randOffset).Unix())

				ctx, cancel := context.WithTimeout(context.Background(), time.Second)
				getDurationMSec.Observe(float64(time.Since(start).Milliseconds()))
				if _, err := client.Get(ctx, key).Result(); err != nil && err != redis.Nil {
					log.Printf("Worker %d error getting key %q: %v", i, key, err)
				}
				cancel()
			}
		}(i)
	}

	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":8080", nil)
}
