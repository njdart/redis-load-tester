FROM golang:1.21-alpine

WORKDIR /build

RUN go install github.com/cosmtrek/air@v1.49.0
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o load-test

CMD ["/build/load-test"]
